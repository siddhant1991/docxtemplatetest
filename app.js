// Requiring module
const express = require('express');
const createReport = require('docx-templates').default;
const fs = require('fs')

// Creating express object
const app = express();

// Handling GET request
app.get('/', async (req, res) => {
    let templatePath = "bill.docx";
    // console.log();
    let templateData = {
        "consignor": "Exporter 123",
        "invoice_number": "BILL/123",
        "invoice_date": "24 May, 2022",
        "address1": "Street 1",
        "address2": "Street 2",
        "address3": "Street 3",
        "address4": "Street 4",
        "address5": "Street 5",
        "taxable_amount": 21000,
        "cgst": 2000,
        "sgst": 2000,
        "igst": 0,
        "round_off": 0,
        "total_amount": 25000,
        "shipments": [
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 },
            { "Consignee": "Sender1", "Destination": "India", "AWB": "QWE456", "Date": "01/05/22", "WT": 34.0, "ND": "N", "Amount": 2100 },
            { "Consignee": "Sender2", "Destination": "USA", "AWB": "ERT567", "Date": "03/05/22", "WT": 16.8, "ND": "D", "Amount": 3300 },
            { "Consignee": "Sender3", "Destination": "Egypt", "AWB": "SDF234", "Date": "05/05/22", "WT": 0.1, "ND": "D", "Amount": 800 }
        ]
    };

    const template = fs.readFileSync(templatePath);
    const buffer = await createReport({
        template,
        data: templateData,
        cmdDelimiter: ['{{', '}}']
    });

    let outputPath = 'report.docx';
    fs.writeFileSync(outputPath, buffer);

    res.send(outputPath)
    res.end()
})

// Port Number
const PORT = process.env.PORT || 5000;

// Server Setup
app.listen(PORT, console.log(
    `Server started on port ${PORT}`));